#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char** argv) 
{
	/* check that there actually is an argument/-s given 
	runs and returns 1 if no args*/
	if (argc == 1){
		printf("wgrep: searchterm [file ...]\n");		
		exit(1);
	}
	/* handle reading from stdin if one argument is given */
	if (argc == 2) {
		/* checked https://stackoverflow.com/questions/9171472/c-getline-how-to-deal-with-buffers-how-to-read-unknown-number-of-values-in/9171511#9171511
		for help with getline() */
		size_t linesiz=0;
		char* linebuf=0;
		ssize_t linelen=0;
		char* term = argv[1];
		while ((linelen=getline(&linebuf, &linesiz, stdin)>1)) {
			/* checked https://stackoverflow.com/questions/12784766/check-substring-exists-in-a-string-in-c
			for reference with substring in string */  			
			if(strstr(linebuf, term) != NULL) {
				printf("%s", linebuf);
			}
		}
		free(linebuf);
		linebuf=NULL;
		exit(0);
	}
	/* iterate over argv, try to open and print file with 
	value of argv[i], exit value 1 if null filepointer */
	int i;
	for (i=2;i<argc;i++){
		FILE *fp = fopen(argv[i], "r");
		if (fp == NULL) {
    			printf("wgrep: cannot open file\n");
    			exit(1);
		}
		size_t linesiz=0;
		char* linebuf=0;
		ssize_t linelen=0;
		char* term = argv[1];
		while ((linelen=getline(&linebuf, &linesiz, fp)>0)) {
			if(strstr(linebuf, term) != NULL) {
				printf("%s", linebuf);
			}
		}
		free(linebuf);
		linebuf=NULL;
		fclose(fp);	
	}
	return 0;
}
