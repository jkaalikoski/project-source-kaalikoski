#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/wait.h>

char error_message[30] = "An error has occurred\n";

/*this function tokenizes the input line, checks if it is a valid command,
runs the command and returns 0 on success, 1 on all errors */
int runLine(char* line, char* path)
{
	/* tokenize input */
	char *token;
	char *delim = " \t";
	int i = 0;
	char *rest = line;
  	char **tokens = malloc (sizeof (char *) * 10); //this means a function can be called with a max of 8 arguments
  	if (!tokens)
		return 1;
	for (i = 0; i < 5; i++) {
		tokens[i] = malloc (30);
		if (!tokens[i]) {
			free (tokens);
			return 1;
		}
	}
	i = 0;
	// tokenize the line of input and add null pointer to end of args list
	while ((token = strtok_r(rest, delim, &rest)) && i<9)
	{
		strncpy(tokens[i], token, 30);
		i++;
	}
	tokens[i] = NULL;
	/* check if first token is a valid command */
	char command[30];
	strncpy(command, path, 30);
	strcat(command, tokens[0]);
	if (access(command, X_OK) != 0)
	{
		write(STDERR_FILENO, error_message, strlen(error_message));
		return 1;
	}
	tokens[0] = command;
	// fork a process and execute the command
	int rc = fork();
	if (rc < 0) { // fork failed; exit
		write(STDERR_FILENO, error_message, strlen(error_message));
		return 1;
	} else if (rc == 0) { // child (new process)
		execv(tokens[0], tokens);
	} else { // parent goes down this path (main)
		wait(NULL);
	}

	return 0;
}


int main(int argc, char** argv)
{
	char path_default[20] = "/bin/";
	char *my_exit = "exit\n";
	int tokens;
	char* redirect = NULL;
	/* if wish was run without batch file enter interactive mode */
	if (argc == 1) 
	{
		/* checked https://stackoverflow.com/questions/9171472/c-getline-how-to-deal-with-buffers-how-to-read-unknown-number-of-values-in/9171511#9171511
		for help with getline() */
		size_t linesiz=0;
		char* linebuf=0;
		ssize_t linelen=0;
		printf("wish> ");
		while ((linelen=getline(&linebuf, &linesiz, stdin)>1)) 
		{
			/* check for exit term "exit" */
			int ret;			
			if((ret=strcmp(linebuf,my_exit))==0) 
			{				
				exit(0);
			}
			char *pos;
			if ((pos=strchr(linebuf, '\n')) != NULL)
				*pos = '\0';
			// check for redirection
			if ((redirect = strchr(linebuf, '>')) != NULL)
			{
				int index = redirect - linebuf;
				linebuf[index] = '\0';
				char *filename = strtok(redirect+1, " ");
				if (!freopen(filename,"w",stdout)){
					write(STDERR_FILENO, error_message, strlen(error_message));
					continue;
				} 
			}
			/* validate commands */
			if ((tokens = runLine(linebuf, path_default)) != 0)
			{
				write(STDERR_FILENO, error_message, strlen(error_message));	
			}
			freopen("/dev/tty", "w", stdout);
			printf("wish> ");
		}
		free(linebuf);
		linebuf=NULL;
		exit(0);
	}
	/* if called with one argument try to enter batch mode */
	if (argc == 2)
	{
		FILE *fp = fopen(argv[1], "r");
		if (fp == NULL) 
		{
    			printf("corrupt batch file\n");
    			exit(1);
		}
		size_t linesiz=0;
		char* linebuf=0;
		ssize_t linelen=0;
		while ((linelen=getline(&linebuf, &linesiz, fp)>0)) 
		{
			char *pos;
			if ((pos=strchr(linebuf, '\n')) != NULL)
				*pos = '\0';
			// check for redirection
			if ((redirect = strchr(linebuf, '>')) != NULL)
			{
				int index = redirect - linebuf;
				linebuf[index] = '\0';
				char *filename = strtok(redirect+1, " ");
				if (!freopen(filename,"w",stdout)){
					write(STDERR_FILENO, error_message, strlen(error_message));
					continue;
				} 
			}
			/* validate commands */
			if ((tokens = runLine(linebuf, path_default)) != 0)
			{
				write(STDERR_FILENO, error_message, strlen(error_message));	
			}
			freopen("/dev/tty", "w", stdout);
		}
		free(linebuf);
		linebuf=NULL;
		fclose(fp);
		exit(0);
	}
	else
	{
		exit(1);
	}
	
}
