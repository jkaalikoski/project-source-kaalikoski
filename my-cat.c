#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) 
{
	/* check that there actually is an argument/-s given 
	runs and returns 0 with no args*/
	if (argc == 1){
		exit(0);
	}
	int i;
	/* using a buffer of 500 chars bc not specified */
	char line[500];
	/* iterate over argv, try to open and print file with 
	value of argv[i], exit value 1 if null filepointer */
	for (i=1;i<argc;i++){
		FILE *fp = fopen(argv[i], "r");
		if (fp == NULL) {
    			printf("wcat: cannot open file\n");
    			exit(1);
		}
		/* checked https://stackoverflow.com/questions/41902471/how-to-use-fgets-to-read-a-file-line-by-line for reference */
		while (fgets(line,500,fp)) {
			printf("%s" , line);		
		}
		fclose(fp);	
	}
	return 0;
}
